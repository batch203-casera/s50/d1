import { Button, Card } from 'react-bootstrap';
import { useState } from 'react';

export default function CourseCard(prop) {
    const [count, setCount] = useState(0);
    console.log(count=5)
    console.log(useState(10))

    return (
        <Card className="my-5 cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h2>{prop.name}</h2>
                </Card.Title>
                <Card.Text>
                    <strong>Description:</strong>
                    <p>{prop.description}</p>
                    <strong>Price:</strong>
                    <p>PHP {prop.price}</p>
                    <Button variant="primary">Enroll Now!</Button>
                </Card.Text>
            </Card.Body>
        </Card>
    )
}
